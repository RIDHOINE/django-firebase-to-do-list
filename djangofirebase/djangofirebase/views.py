from django.shortcuts import render 
import pyrebase
# Configuration BDD
config = {
    "apiKey": "AIzaSyAz5Y6A3lMcWLhSCRdha3WiopUn4FMnC44",
    "authDomain": "django-firebase-bf13f.firebaseapp.com",
    "databaseURL": "https://django-firebase-bf13f.firebaseio.com",
   " projectId": "django-firebase-bf13f",
    "storageBucket": "django-firebase-bf13f.appspot.com",
    "messagingSenderId": "1062946871274",
    "appId": "1:1062946871274:web:b1b0cfb7f392cc943d8493"
}

# Initialisation de l'application 
firebase = pyrebase.initialize_app(config)
auth = firebase.auth()

# Fonction signIn()
def singIn(request):
    return render(request, "signIn.html")

# Fonction postsign()
def postsign(request):
    email=request.POST.get('email')
    passw = request.POST.get("pass")

    try:
        user = auth.sign_in_with_email_and_password(email,passw)
    except:
        message = "invalid crediantials"  
        return render(request, "signIn.html", {"msg":message})
        
    print(user)
    return render(request, "welcome.html",{"e":email})